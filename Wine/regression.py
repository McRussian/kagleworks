import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

data = pd.read_csv('../Data/winequalityN.csv')
data.loc[data['type'] == 'red', 'type'] = 0
data.loc[data['type'] == 'white', 'type'] = 1

print(data.head(15))