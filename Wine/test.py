import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

data = pd.read_csv('../Data/winequalityN.csv')
print(data.head(10))
print(data.columns)
print(data.dtypes)

print(data.corr())

print(data.type.unique())
